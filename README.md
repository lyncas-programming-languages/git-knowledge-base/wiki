# Orientações de Git, GitHub e GitFlow

  <p align="center">
    <img src="docs/lyncas_fundo_azul.png" alt="Lyncas"/>
  </p>

> Versão do documento: v1.0  
> Criado em: 21/12/2022  
> Criado por: Jonas Gustavo Radünz  
> **Plugin** para exibir este arquivo corretamente no **Google Chrome**: *Markdown Reader*

- **Git**: ferramenta de versionamento (controle de versão) de código. *Download* através do site oficial;
- **GitHub**: plataforma *WEB* que serve como um repositório de código/projetos (públicos ou privados) e que utiliza recursos do Git.
- **GitFlow**: modelo de gerenciamento de *branches* (ramificações) do Git.  

<p align="center">
	<img src="https://pbs.twimg.com/media/C3maMplVYAAv-mf.jpg:small" alt="Final.doc"/>
</p>
<p align="center">
	<span>Fonte: <a href="https://phdcomics.com/comics.php?f=1531">PhD Comics</a></span> 
</p>

## Configurações iniciais

Usando o **terminal** do sistema operacional (ou de IDEs como o *Visual Studio*, por exemplo):  
``` git --version``` para verificar se o Git está instalado.  
``` git``` para mostrar uma lista de comandos.

``` git init``` para inicializar o Git em determinado projeto/diretório.  
**Porém**, é interessante iniciar o Git com um *branch* chamado *main* (para igualar ao nome do branch quando o repositório for criado). Para isso, usar o comando ```git init -b main```.  
Então, é criada uma pasta **.git** (oculta, mas que pode ser exibida no VS se alteradas as devidas configurações).

Ao iniciar um repositório, **configurar o usuário local**:  
```git config user.name "userteste"```  
```git config user.email "contato@teste.com.br"```  

Também pode ser usada a *flag* **global** na configuração do usuário (usá-la apenas na própria máquina; não em máquinas públicas, como servidores). Ex.: ```git config --global user.name "userteste"```

Posteriormente, o usuário pode ser verificado no arquivo **config**, na pasta .git ou pelo comando ```git config –-list```.

### Após um repositório ser criado
```git remote add origin <URL do repositório>``` irá definir o repositório remoto.  
```git remote -v``` para verificar links do repositório remoto (*origin*).  

### Credenciais (usuário e senha)
```git config credential.helper store``` para armazenar.  
```git config --global --unset credential.helper``` para remover.   
<span style="color:red">Obs.: a senha fica armazenada em formato de texto nas configurações do Git.</span>    
<span style="color:red">Indicar como fazer via SSH.</span>  
Ver: <https://www.youtube.com/watch?v=5jwzAhcovMU> e <https://www.youtube.com/watch?v=kHkQnuYzwoo&t=475s>  

## Comandos iniciais

- Comando **add**
	- ```git add README.md``` para adicionar apenas este arquivo para ser submetido no novo commit.
	- ```git add .``` para adicionar todos os arquivos novos/modificados para serem submetidos no novo commit.
- Comando **commit**: para preparar arquivos para envio ao respectivo *branch*. 
	- Ao ser criado um commit, é feito um novo *snapshot* do projeto;
	- Cada commit possui um **id** (7 primeiros caracteres do *hash*).
	- **Dica**: fazer vários commits ao longo do dia, a cada atividade realizada, para facilitar os retornos a *snapshots* anteriores do código.
- Comando **status**: para verificar o que está acontecendo no projeto (opcional, pois tmbém é possível visualizar na IDE).

### Exemplo nº 1 de primeiro commit
Obs.: os comandos *pull* e *push* e conceitos de *branches* serão detalhados posteriormente.  

- Criar repositório no *GitHub* (usar licença MIT);
- Criar pasta para o repositório com um novo arquivo README.md;
- Executar os seguintes comandos via *Git bash here*:
	- ```git init -b main``` (para iniciar o repositório já no branch *main*, conforme *GitHub*)
		- Obs.: também é possível renomear o branch *main* para *master* no *GitHub* e usar o comando ```git init``` apenas.
	- ```git config user.name "nome-do-usuario"```
	- ```git config user.email "email-do-usuario@dominio.com"```
	- ```git remote add origin https://github.com/nome-do-usuario-do-github/nome-do-repositorio```
	- ```git pull origin main``` (para atualizar o repositório local antes da criação de qualquer *commit*)
	- ```git add .```
	- ```git commit -m "First commit. Creation of README.md file."```
	- ```git push origin main``` (para atualizar o repositório remoto)

### Exemplo nº 2 de primeiro commit

1. Criar o arquivo **.gitignore** na raiz do projeto. Sua função é servir de filtro na monitoração e upload. Conteúdo:  
	> /node_modules  
	> /docs  
	> bin/  
	> docs/  
	> obj/  
	> *.user  
	> .vs/  
	> *.log*  
2. Executar os comandos a seguir no terminal, na pasta do projeto.
	```
	git add .gitignore
	git commit -m "Commit inicial, adicionado o arquivo .gitignore na raiz do projeto."
	git status
	git log
	```

### Comando **log**

Utilizado para verificar o histórico de commits do branch. Também é possível visualizar na IDE.
- Se a consulta do log for finalizada com ```:```, foram apresentados apenas os commits que couberam no terminal;
- ```git config core.pager cat``` para mostrar todos os commits do log no terminal;
- Alterar ```:``` para ```/inicial``` para procurar commits com o texto inicial na mensagem;
- ```b``` para voltar e ```q``` para sair;
- Outras opções com o comando **log**:  
	```git log -2``` para mostrar os 2 últimos logs.  
	```git log --oneline``` para mostrar um resumo, com id + mensagem na mesma linha.  
	```git log --oneline -2```  
	```git log --before="2020-05-16"```  
	```git log --after="2020-05-16"```  
	```git log --since="2 days ago"```  
	```git log --after="1 week ago"```  
	```git log --author="userteste"```  
	```git log --author="teste"```  
	```git help log``` para mais opções de comandos de log.  

### **Comandos adicionais**

- Comando **diff** para visualizar diferenças no código entre dois arquivos (geralmente feito via interface em *Code Review*).
	- Após a adição de arquivo(s), mas antes do commit:  
	```git diff --staged``` para visualizar as alterações de *staged* (adicionados) em relação ao último commit do respectivo arquivo.  
	```git diff <id do commit>``` para visualizar as alterações de *staged* (adicionados) em relação ao commit do id informado.  
	- Diferenças entre dois commits:
	```git diff <id do commit mais antigo>..<id do commit mais novo>```

- Comando **mv** (move) para renomear arquivos (ou diretórios).
Se o arquivo for renomeado diretamente na IDE, será necessário adicioná-lo antes do commit.
É possível verificar esta diferença através do comando ```git status```.  
Exemplo: ```git mv programa1.html programa2.html```.  

- Comando **rm** (remove) para remover arquivos.
Se o arquivo dor excluído diretamente na IDE, será necessário adicioná-lo antes do commit.
É possível verificar esta diferença através do comando ```git status```.  
Exemplo: ```git rm programa3.html programa4.html```.

- Comando **commit** (*extras*):
	- Usar flag *--amend* para alterar o último commit. Exemplos:
	```
	git commit -m "Mensagem incorreta"
	git commit --amend -m "Mensagem correta"
	```
	```
	git commit -m "Faltando o arquivo programa5.html"
	git add programa5.html
	git commit --amend -m "Com o arquivo programa5.html"
	```

- Comando **restore** para tirar os arquivos de *staged* (quando já foram adicionados e estão prontos para o commit):
	```
	git add programa6.html
	git status
	git restore programa6.html
	git status
	```

Para voltar todas as modificações (antes de adicioná-las) ao estado do último commit:  
```git reset HEAD --hard``` (HEAD é o último commit; hard = pode sobrescrever).

Exemplo com três arquivos **adicionados** ao staged **e comitados** (descartar último commit):  
```git reset HEAD^ --hard``` (circunflexo = voltar para o último commit).

#### Comandos adicionais de commits
```git revert <commit id>``` cria um novo commit desfazendo as alterações do commit informado.  
Assim, o histórico é mantido, com o commit revertido e o novo.  
É necessário fazer ```git push``` após o ```git revert```.

Também é possível utilizar o ```git reset```. Entretanto, assim não se mantém o histórico (o commit é removido).  
Exemplos:  
```git reset HEAD~1``` apenas o último commit é desfeito.   
```git reset HEAD~2``` os últimos dois commits são desfeitos.  

## Trabalhando com branches (ramificações)

```git branch``` para verificar os branches existentes no projeto (* indica o branch atual, também descrita no git status).
- **master** é o branch (ramo) principal.  
```git branch feature/funcionalidadeA``` para criar este branch, porém permanecendo no branch atual (master).  
- Comando **checkout**: para mudar de branch (**-b** para criar um branch e já utilizá-lo). Exemplos:  
	```git checkout feature/funcionalidadeA```  
	```git checkout master```  
	```git checkout -b develop```  
	```git checkout -b feature/funcionalidadeB```  

	- Outras possibilidades com **checkout**:
		- ```git checkout <id do commit> -- .``` para voltar ao estado de algum commit específico.
		- ```git checkout -- .``` para voltar todos os arquivos não adicionados ao estado original.
		- ```git checkout -- teste.txt``` para voltar apenas este arquivo não adicionado ao estado original.
		- ```git checkout HEAD -- .``` para voltar todos os arquivos já adicionados ao estado original.
		- ```git checkout HEAD -- teste.txt``` para voltar apenas este arquivo já adicionado ao estado original.

```git branch -d feature/funcionalidadeA``` para excluir o branch do repositório local **com** verificação de merge.  
```git branch -D feature/funcionalidadeA``` para excluir o branch do repositório local **sem** verificação de merge.  

Realizando **merge** (*feature/funcionalidadeB* => *develop*):  
Obs.: É no momento do merge que costumam ocorrer os conflitos.  
```
git checkout develop
git merge feature/funcionalidadeB
git branch -d feature/funcionalidadeB
```
É importante que os branches sigam um padrão de fluxo e nomenclaturas.  
Por isso, posteriormente será apresentado o **GitFlow**.  

## Usando alias no Git:
Referência: <https://git-scm.com/book/en/v2/Git-Basics-Git-Aliases>
```
git config --global alias.co checkout
git config --global alias.br branch
git config --global alias.ci commit
git config --global alias.st status
```

## Avançando nos comandos Git

### **Clone** e **Push** 
```git clone <URL do repositório remoto>``` ou ```git clone <caminho local>``` para copiar de algum diretório existente.  
- É possível clonar repositórios públicos, mas apenas colaboradores do projeto podem realizar push.  
- Não é possível realizar push em um non-bare repository (por exemplo, um repositório local).  
- GitHub é um *bare repository*.
	
### **Bare repository**
Bare repository = repositório vazio. Trata-se do repositório principal, em um servidor, por exemplo.  
```git init --bare``` não é criado o .git como oculto, mas com arquivos extraídos e não ocultos.  

### **Rebase**
Parecido com o merge, porém é encaixado no ponto de onde o branch foi criado.  
Ex.: ```git rebase feature/funcionalidadeC```  

### **Fetch** e **Pull**  

**Fetch**: baixa arquivos sem fazer o merge, possibilitando posterior *rebase*.  
- ```git fetch``` no repositório clonado.  
- ```git rebase``` para fazer o rebase ao invés do merge.  

**Pull**:  
- ```git pull``` = git fetch + git merge.  
- ```git pull --rebase``` = git fetch + git rebase.  

<span style="color:orange"><b>Atenção</b></span> às mensagens do git! Por exemplo, ao executar o comando ```git pull```, pode ser indicado conflito em algum arquivo, sem necessariamente ocorrer um erro. Após solucionar um conflito, o arquivo deve ser **adicionado**, **comitado** e feito o **push** novamente.

## **Tags**
Utilizados para definir versões estáveis do projeto (*entregáveis*, muito usados em metodologias ágeis, como SCRUM).
**Não se comita em tags**, é apenas para visualizar o estado de um projeto por outros usuários ou facilitar a visualização do branch master de uma determinada versão.  

```git tag``` apresenta todos os tags do projeto.  

Por exemplo, ao definir o tag v1.0.0:
```
git tag v1.0.0
git push origin v1.0.0
git checkout v1.0.0
```
```
git checkout v1.0.0
```

Para criar um branch a partir de um *tag*, usar o comando ```git switch -c <nome do novo branch>```.  
Ex.: ```git switch -c correcoes-v1.0.0```

## Utilização de *Plataformas WEB* (GitHub)

### *Issues*:
- Podem ser criadas por **usuários que não são donos ou colaboradores** de determinado repositório. Porém, não é possível atribuir ou criar *label* se não for este tipo de usuário;  
- É possível fechar issues automaticamente através dos commits.
	- Ex.: na descrição do commit, para fechar a issue #1: "Corrigindo erro de typo (digitação). Closes #1"
	
### *Fork*:
Para "copiar" o repositório de outro usuário para o usuário que fez o *fork*.  
Assim, ele se torna **independente** do repositório clonado.

### *Gists*:
Para salvar referências pouco utilizadas (por exemplo, trechos de código com alguma funcionalidade específica).

### *Pull Request*:
Em repositórios públicos, é possível abrir um PR após realizar o fork deste repositório, se tornando assim um colaborador indireto do projeto.

## Extras

- [Orientações para a criação do *README.md*;](https://docs.github.com/pt/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax)
- [Semantic Versioning;](https://semver.org/)
- Sugestão de **plugins** do git para **VS Code**: GitLens e Git History;
- Pendências (*Leonardo Güths*):
	- [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)
	- [Semantic Release (npm)](https://www.npmjs.com/package/semantic-release)

## GitFlow (Teoria)
Referências:  
- [Atlassian - Fluxo de trabalho de Gitflow](https://www.atlassian.com/br/git/tutorials/comparing-workflows/gitflow-workflow)  
- [Medium - Diego Felix - GitFlow - Um fluxo de trabalho GIT simples e seguro.](https://blog.wkm.com.br/gitflow-um-fluxo-de-trabalho-git-simples-e-seguro-5445fc70e08b)  
- [YouTube - Angelo Luz - Git - GitFlow na prática](https://www.youtube.com/watch?v=wzxBR4pOTTs)  

<p align="center">
	<img src="https://lh3.googleusercontent.com/70jaEZnESXQ6SssU5uI4yO62JBz6xq2sNrrz8bW_ap2CuWUaQlbKs3j6NyRJnvcvYwAugkW8WzNJX21dZ2SMd9O_1TTpKZT-FsBkYSPy4rUSpJSo2C-WPTaLc2jQ8ancyj1TetXQ" alt="GitFlow"/>
</p>
<p align="center">
	<span>Fonte: <a href="https://blog.betrybe.com/git/git-flow/">Trybe - Git Flow: o que é e como gerenciar branches?</a></span> 
</p>

- O GitFlow é um modelo de gerenciamento de branches (ramificações) do Git para ser usado como **base**, respeitando-se os padrões de cada cliente.
	- O branch *Master* equivale ao ambiente de produção (*PRD*).
	- Em alguns clientes, pode existir um branch de *Release* permanente (que não é excluída) para o ambiente de homologação (*QAS*).
	- Alguns nomes podem apresentar variações (*Main* ao invés de *Master* e *Homolog* ao invés de *Release*, por exemplo).
	- Os branches de *Feature*, *Release* e *Hotfix* são excluídas após a aprovação e merge.
- A imagem acima é auto-explicativa e representa os procedimentos adotados no GitFlow.
	- Diferentemente da imagem, **sempre** realizar pull da *Develop* antes do merge da *Feature* c/ a *Develop*.  
	- Se houver um *Hotfix* durante a existência de uma release, merge *hotfix => release* e merge *release => develop*.  
- Apesar de se prevenir erros de nomenclatura e de fluxo, **não** utilizar os comandos do *GitFlow* no terminal. Ou seja, utilizar apenas a definição do fluxo, pois o *Code Review* é realizado nas *Plataformas WEB*.

### Semântica dos branches
Fonte: [Medium - Diego Felix - GitFlow - Um fluxo de trabalho GIT simples e seguro.](https://blog.wkm.com.br/gitflow-um-fluxo-de-trabalho-git-simples-e-seguro-5445fc70e08b)

Cada branch tem seu propósito e regras associadas:
- Master
	- Atemporal, reflete o código de produção.
	- Cada merge/commit no master deve ser passível de implantação em produção.
	- Uso de tags para marcar as atualizações, preferencialmente indicando a versão. Por exemplo, “3.0.2”.
	- Criado na inicialização do GitFlow.
- Develop
	- Atemporal, é o histórico do desenvolvimento.
	- Linha de integração. As features são juntadas nesse branch.
	- Os commits não deixam a linha necessariamente estável. O ideal é que sejam rodados testes completos a cada commit nesse branch.
	- Quando se alcança um estado entregável, o código desta linha deve ser colocado em master, através de um release.
	- Criado na inicialização do GitFlow.
- Feature
	- Volátil, representa as novas funcionalidades que estão sendo desenvolvidas.
	- Iniciado a partir de develop. No seu fechamento, o merge é feito em develop também.
- Release
	- Geração de pacotes para produção.
	- Quando se abre um branch de release, ou tudo que está nele é aprovado ou tudo que está nele é reprovado. Pequenos ajustes podem ser feitos no próprio release que está aberto.
	- Criado a partir de develop. Quando fechado, é feito um merge no develop e outro no master.
- Hotfix
	- Linha de correções.
	- Geração de bugfixes. Criado quando um erro é encontrado e deve ser corrigido em produção.
	- Criado a partir de master. Quando fechado, é feito um merge no develop e outro no master.

## GitFlow (Resumo)  
Fonte: [Atlassian - Fluxo de trabalho de Gitflow](https://www.atlassian.com/br/git/tutorials/comparing-workflows/gitflow-workflow)

> 1. Um branch *develop* é criado a partir do *master*;
> 2. Um branch de *release* é criado a partir do *develop*;
> 3. Um branch de *feature* é criado a partir do *develop*;
> 4. Quando uma *feature* é concluída, é feito o merge do branch no *develop*;
> 5. Quando o branch *release* é aprovado, é feito o merge deste no *develop* e no *master*;
> 6. Se for detectado um problema no *master*, é criado um branch *hotfix* a partir do *master*;
> 7. Quando o branch *hotfix* é aprovado, é feito o merge deste no *develop* e no *master*;

### (Algumas) Boas práticas  
Fonte: [Medium - Diego Felix - GitFlow - Um fluxo de trabalho GIT simples e seguro.](https://blog.wkm.com.br/gitflow-um-fluxo-de-trabalho-git-simples-e-seguro-5445fc70e08b)

> - Sempre saiba o que está fazendo!
> - Mantenha a linha develop estável!
> - Code review antes de cada merge!
> - Testes depois de cada merge!
> - Não confie cegamente no merge automático!
> - Não saia do Fluxo! Se perceber que saiu, volte ao fluxo o mais rápido! Peça ajuda, se necessário.
> - Mantenha a timeline do projeto limpa!
> - Use ferramentas que auxiliem no controle do Fluxo!

## GitFlow (Prática)

<p align="center">
	<img src="https://miro.medium.com/max/1200/1*tmVNUoA7AM43x0IuuxDOMg.jpeg" alt="GitFlow"/>
</p>
<p align="center">
	<span>Fonte: <a href="https://blog.wkm.com.br/gitflow-um-fluxo-de-trabalho-git-simples-e-seguro-5445fc70e08b">Medium - Diego Felix - GitFlow - Um fluxo de trabalho GIT simples e seguro.</a></span> 
</p>

### Procedimento inicial:

1. Criar o branch *develop*:  
```
git init
git config user.name ""
git config user.email ""
git config –list
git remote add origin https://gitlab.com/lyncas-dev-jr/projetoteste.git
git checkout -b develop
```

2. Criar os primeiros arquivos do repositório. Após criados:  
```
git add .
git commit -m "Commit inicial."
git push origin develop
```

### Procedimento para novas *features*:

1. Criar o branch da *feature* a partir do branch *develop*:  
```
# Para definir o branch a ser usada como base:  
git checkout develop 

# Para atualizar o branch a ser usada como base:  
git pull origin develop 

# Para criar o branch:  
git checkout -b feature/nome-do-que-sera-feito 
```

2. Realizar as devidas imprementações/alterações no branch *feature/nome-do-que-sera-feito* e enviá-las à plataforma WEB:  
```
git add .
git commit -m "Criada a tela de cadastro xyz."
git pull origin develop
git push origin feature/nome-do-que-sera-feito
```

3. Criar o *merge request* (ou *pull request*) na plataforma WEB.  

4. Se estiver(em) de acordo, o(s) revisor(es) irão aprovar o *merge request* e excluir o branch *feature/nome-do-que-sera-feito*.  

### Usando o GitLab:

1. Criar o merge request do branch de feature para develop **(NUNCA REALIZAR MERGE DA FEATURE PARA A MAIN OU PARA A MASTER).**
2. Aprovar o merge request clicando em “Merge” (Nunca em “Close merge request”).  